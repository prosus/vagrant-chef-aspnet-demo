﻿using System;

using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;


public partial class _Default : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			DataTable dt = this.GetData();


			StringBuilder html = new StringBuilder();

			html.Append("<table border = '1'>");

			html.Append("<tr>");
			foreach (DataColumn column in dt.Columns)
			{
				html.Append("<th>");
				html.Append(column.ColumnName);
				html.Append("</th>");
			}
			html.Append("</tr>");

			foreach (DataRow row in dt.Rows)
			{
				html.Append("<tr>");
				foreach (DataColumn column in dt.Columns)
				{
					html.Append("<td>");
					html.Append(row[column.ColumnName]);
					html.Append("</td>");
				}
				html.Append("</tr>");
			}

			html.Append("</table>");

			ContactTableData.Controls.Add(new Literal { Text = html.ToString() });
		}
	}

	private DataTable GetData()
	{
		string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

		using (SqlConnection con = new SqlConnection(constr))
		{
			using (SqlCommand cmd = new SqlCommand("SELECT FirstName, LastName, EmailAddress FROM Contact"))
			{
				using (SqlDataAdapter sda = new SqlDataAdapter())
				{
					cmd.Connection = con;
					sda.SelectCommand = cmd;
					using (DataTable dt = new DataTable())
					{
						sda.Fill(dt);
						return dt;
					}
				}
			}
		}
	}
}