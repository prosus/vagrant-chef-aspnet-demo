### Example of a very simple ASP.net app deployment ###

This will deploy a vagrant box containing:

* IIS
* Sql Server Express 2012
* A very basic ASP.net website

OS: Windows Server 2012 R2

Configuration Managment: Chef Zero

Database Tier: SQL Server 2012 Express

Web Tier: IIS/ASP.net
