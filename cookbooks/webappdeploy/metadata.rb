name 'webappdeploy'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures webappdeploy'
long_description 'Installs/Configures webappdeploy'
version '0.1.0'

depends 'sql_server', '~> 2.6.2'