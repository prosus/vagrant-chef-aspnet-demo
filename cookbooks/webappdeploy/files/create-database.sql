USE master;
GO

CREATE DATABASE DemoDatabase;
GO
USE DemoDatabase;
GO

CREATE TABLE Contact(
  Id uniqueidentifier NOT NULL DEFAULT NEWID(),
  PRIMARY KEY(id),
  FirstName VARCHAR(100),
  LastName VARCHAR(100),
  EmailAddress VARCHAR(100)
);
GO
-- Chuck in some demo data
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('John', 'Doe', 'JohnDoe@somewhere.com');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Jane', 'Smith', 'JaneSmith@overthere.co.uk');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Fred', 'Doe', 'FredDoe@somewhere.com');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Freda', 'Smith', 'FredaSmith@overthere.co.uk');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Mary', 'Doe', 'MaryDoe@somewhere.com');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Kate', 'Smith', 'KateSmith@overthere.co.uk');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Sam', 'Doe', 'SamDoe@somewhere.com');
INSERT INTO Contact(FirstName, LastName, EmailAddress) VALUES('Pete', 'Smith', 'PeteSmith@overthere.co.uk');
GO