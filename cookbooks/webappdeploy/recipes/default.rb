#
# Cookbook Name:: webappdeploy
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'webappdeploy::configuredb'
include_recipe 'webappdeploy::deploywebapp'