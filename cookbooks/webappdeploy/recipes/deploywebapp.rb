#
# Cookbook Name:: webappdeploy
# Recipe:: deploywebapp
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
powershell_script 'Deploy Web Application' do
  code <<-EOH
		Copy-Item "C:\\vagrant\\webapp\\*" "C:\\inetpub\\wwwroot" -Force -Recurse

		Remove-Item "C:\\inetpub\\wwwroot\\iisstart.htm" -Force
  EOH
end