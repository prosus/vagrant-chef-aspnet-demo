#
# Cookbook Name:: webappdeploy
# Recipe:: configuredb
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
# Install SQL Server.
include_recipe 'sql_server::server'

# Create a path to the SQL file in the Chef cache.
create_database_script_path = win_friendly_path(File.join(Chef::Config[:file_cache_path], 'create-database.sql'))
grant_permissions_script_path = win_friendly_path(File.join(Chef::Config[:file_cache_path], 'grant-permissions.sql'))

# Copy the SQL files from the cookbook to the Chef cache.
cookbook_file create_database_script_path do
  source 'create-database.sql'
end

cookbook_file grant_permissions_script_path do
  source 'grant-permissions.sql'
end

# Get the full path to the SQLPS module.
sqlps_module_path = ::File.join(ENV['programfiles(x86)'], 'Microsoft SQL Server\110\Tools\PowerShell\Modules\SQLPS')

powershell_script 'Initialize database' do
  code <<-EOH
    Import-Module "#{sqlps_module_path}"
    Invoke-Sqlcmd -InputFile #{create_database_script_path}
    Invoke-Sqlcmd -InputFile #{grant_permissions_script_path}
  EOH
  guard_interpreter :powershell_script
  only_if <<-EOH
    Import-Module "#{sqlps_module_path}"
    (Invoke-Sqlcmd -Query "SELECT COUNT(*) AS Count FROM sys.databases WHERE name = 'DemoDatabase'").Count -eq 0
  EOH
end